<?php
// https://mlocati.github.io/php-cs-fixer-configurator/#version:3.0

$finder = PhpCsFixer\Finder::create()->in(['src']);

$config = new PhpCsFixer\Config();

return $config->setRules(['@PSR12' => true])->setFinder($finder);
