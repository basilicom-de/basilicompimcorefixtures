#!/usr/bin/make -f

PROJECT_NAME := imos-pim
SHELL := /bin/bash

UID = $(shell id -u)
GID = $(shell id -g)

cwd := $(shell pwd)

DOCKER_PHP := basilicom/php-fpm-pimcore:8.2
DOCKER_NODE := node:20-alpine

PROJECT_DIR = $(cwd)
TMP_BUILD_DIR = $(cwd)/tmp

$(TMP_BUILD_DIR):
	mkdir -p $(TMP_BUILD_DIR) $(TMP_BUILD_DIR)/logs $(TMP_BUILD_DIR)/dist

define run_in_workspace
	docker compose run -e COLUMNS=$$(tput cols) -e LINES=$$(tput lines) --user www-data php-fpm /bin/bash -c "cd /php && $(1)"
endef

.PHONY: help
help: ## Show help
	@IFS=$$'\n' ; \
    help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//'`); \
    for help_line in $${help_lines[@]}; do \
        IFS=$$'#' ; \
        help_split=($$help_line) ; \
        help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        printf "%-30s %s\n" $$help_command $$help_info ; \
    done

.PHONY: composer-install
composer-install: ## Install php dependencies
	$(call run_in_workspace,COMPOSER_MEMORY_LIMIT=-1 composer install)

.PHONY: composer-update
composer-update: ## Update php dependencies
	$(call run_in_workspace,COMPOSER_MEMORY_LIMIT=-1 composer update)

.PHONY: lint
lint: security-check php-cs-fixer-dry

.PHONY: php-cs-fixer
php-cs-fixer:
	@printf "\nExecuting php-cs-fixer\n"
	$(call run_in_workspace,vendor/bin/php-cs-fixer fix --verbose --config=.php-cs-fixer.dist.php)

.PHONY: php-cs-fixer-dry
php-cs-fixer-dry:
	@printf "\nExecuting php-cs-fixer in dry-run\n"
	$(call run_in_workspace,vendor/bin/php-cs-fixer fix --verbose --config=.php-cs-fixer.dist.php --dry-run)

##### Security Checker #####
UNAME_S := $(shell uname -s)
SEC_CHECK_URL := "https://github.com/fabpot/local-php-security-checker/releases/download/v1.0.0/local-php-security-checker_1.0.0_"

ifeq ($(UNAME_S),Linux)
	SEC_CHECK_URL:="$(SEC_CHECK_URL)linux_amd64"
else ifeq ($(UNAME_S),Darwin)
	SEC_CHECK_URL:="$(SEC_CHECK_URL)darwin_amd64"
endif

.PHONY: security-check
security-check: $(TMP_BUILD_DIR)
	@wget ${SEC_CHECK_URL} -O $(PROJECT_DIR)/vendor/bin/local-php-security-checker && chmod a+x $(PROJECT_DIR)/vendor/bin/local-php-security-checker
	$(PROJECT_DIR)/vendor/bin/local-php-security-checker --path=$(PROJECT_DIR)/composer.lock | tee $(TMP_BUILD_DIR)/security-check.log || (echo "security-check failed $$?"; exit 1)
############################
