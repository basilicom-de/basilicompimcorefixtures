<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\HttpKernel\Kernel;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(
            'basilicom_pimcore_fixtures'
        );

        // bc compat
        if (version_compare(Kernel::VERSION, '4.2.0') === -1) {
            $treeBuilder->root('basilicom_pimcore_fixtures');
        }

        return $treeBuilder;
    }
}
