<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class PropertyHydratorChainCompilerPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    public function process(ContainerBuilder $container)
    {
        if (!$container->has('basilicom_pimcore_fixtures.property_hydrator_chain')) {
            return;
        }

        $definition = $container->findDefinition(
            'basilicom_pimcore_fixtures.property_hydrator_chain'
        );

        $taggedServices = $this->findAndSortTaggedServices(
            'basilicom_pimcore_fixtures.property_hydrator',
            $container
        );

        foreach ($taggedServices as $reference) {
            $definition->addMethodCall(
                'addHydrator',
                [$reference]
            );
        }
    }
}
