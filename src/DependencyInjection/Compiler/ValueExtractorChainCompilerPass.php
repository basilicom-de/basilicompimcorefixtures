<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ValueExtractorChainCompilerPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    public function process(ContainerBuilder $container)
    {
        if (!$container->has('basilicom_pimcore_fixtures.value_extractor_chain')) {
            return;
        }

        $definition = $container->findDefinition(
            'basilicom_pimcore_fixtures.value_extractor_chain'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'basilicom_pimcore_fixtures.value_extractor'
        );

        foreach ($taggedServices as $id => $tags) {
            // a service could have the same tag twice
            foreach ($tags as $attributes) {
                $fields = array_key_exists('fields', $attributes) ? explode('|', $attributes['fields']) : [];

                $definition->addMethodCall('addExtractor', [
                    new Reference($id),
                    $fields,
                ]);
            }
        }
    }
}
