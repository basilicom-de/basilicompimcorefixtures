<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Command;

use Basilicom\PimcoreFixtures\Generation\FileSort\FileSortInterface;
use Basilicom\PimcoreFixtures\Repository\FolderRepository;
use Basilicom\PimcoreFixtures\Service\FixtureGenerator;
use Basilicom\PimcoreFixtures\Service\FixtureLoader;
use Exception;
use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject\Folder;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class GenerateFixturesCommand extends AbstractCommand
{
    public const COMMAND = 'basilicom:pimcore:fixtures:generate';

    protected FixtureGenerator $fixtureGenerator;

    public function __construct(FixtureGenerator $fixtureGenerator, string $name = null)
    {
        $this->fixtureGenerator = $fixtureGenerator;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND)
            ->setDescription('Generate yml fixtures')
            ->addOption(
                'file-sort',
                null,
                InputOption::VALUE_OPTIONAL,
                'Custom sorting implementation for sorting the fixture files.'
            );
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fileSorterClass = $input->getOption('file-sort') ?? null;

        $fileSorter = null;
        if (!is_null($fileSorterClass)) {
            if (!class_exists($fileSorterClass)) {
                throw new Exception('Expected an existing class name. Got: ' . $fileSorterClass);
            }

            $fileSorter = new $fileSorterClass();
            if (!($fileSorter instanceof FileSortInterface)) {
                throw new Exception(
                    sprintf(
                        'Expected an instance of %s. Got: %s',
                        FileSortInterface::class,
                        get_class($fileSorter)
                    )
                );
            }
        }

        $helper = $this->getHelper('question');

        $folderRootQuestion = new ChoiceQuestion(
            '<info>Choose root folder?</info>',
            $this->formatFoldersToCommandChoices(),
            0
        );

        $rootFolder = $helper->ask($input, $output, $folderRootQuestion);

        $levelsQuestion = new Question('<info>Choose max levels deep (100): </info>', 100);
        $levels = (int)$helper->ask($input, $output, $levelsQuestion);

        $output->writeln(
            [
                '<info>',
                'You chose: ',
                'Root folder: <comment>' . $rootFolder . '</comment>',
                'Max level deep: <comment>' . $levels . '</comment>',
                '</info>',
            ]
        );

        $confirmationQuestion = new ConfirmationQuestion(
            '<info>Continue with this action? (y)</info>'
        );

        if (!$helper->ask($input, $output, $confirmationQuestion)) {
            return self::FAILURE;
        }

        // cleanup
        $this->cleanUp();

        $rootId = Folder::getByPath($rootFolder)->getId();
        try {
            $this->fixtureGenerator->setFolder(Folder::getById($rootId));
            $this->fixtureGenerator->setMaxLevels($levels);

            if (!is_null($fileSorter)) {
                $output->writeln('Using custom file sorter: ' . $fileSorterClass);
                $this->fixtureGenerator->setFileSorter($fileSorter);
            }

            $this->fixtureGenerator->generateFixturesForFolder();
        } catch (Exception $e) {
            $output->writeln('<error>Fixture generation failed.</error>');
            $output->writeln($e->getMessage());
            exit(1);
        }

        $output->writeln('<info>Done. Your fixtures are at: "' . FixtureLoader::DEFAULT_FIXTURE_FOLDER . '".</info>');

        return self::SUCCESS;
    }

    private function formatFoldersToCommandChoices(): array
    {
        $foldersRepo = new FolderRepository();
        $folders = $foldersRepo->getFoldersByQuery();
        $foldersArr = [];
        foreach ($folders as $folder) {
            $foldersArr[] = $folder->getFullPath();
        }

        return $foldersArr;
    }

    private function cleanUp(): void
    {
        foreach (glob(FixtureLoader::DEFAULT_FIXTURE_FOLDER . '_generated' . DIRECTORY_SEPARATOR . '*.yml') as $file) {
            unlink($file);
        }
    }
}
