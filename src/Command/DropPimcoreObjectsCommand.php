<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Command;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use InvalidArgumentException;
use Pimcore\Cache;
use Pimcore\Console\AbstractCommand;
use Pimcore\Db;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DropPimcoreObjectsCommand extends AbstractCommand
{
    public const COMMAND      = 'basilicom:pimcore:fixtures:drop-objects';
    public const FORCE_FLAG   = 'force';
    public const EXCLUDE_FLAG = 'exclude';
    public const DROP_FOLDER  = 'drop-folder';

    protected Connection $db;
    protected array $additionalTables;

    public function __construct(string $name = null, array $additionalTables = [])
    {
        parent::__construct($name);
        $this->db = Db::get();
        $this->additionalTables = [
            ...$additionalTables,
            ...[
                'notes',
                'notes_data',
                'edit_lock',
                'search_backend_data',
                'recyclebin',
                'dependencies',
                'versions',
            ],
        ];
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND)
            ->setDescription('Drops ALL pimcore objects. Be aware what you are doing!')
            ->addOption(
                self::FORCE_FLAG,
                null,
                InputOption::VALUE_NONE,
                'Force the operation. Be aware what you are doing!'
            )
            ->addOption(
                self::DROP_FOLDER,
                null,
                InputOption::VALUE_OPTIONAL,
                'Use to drop folders too.',
                false
            )
            ->addOption(
                self::EXCLUDE_FLAG,
                null,
                InputOption::VALUE_OPTIONAL,
                'List of object class names to exclude from deletion.',
                null
            );
    }

    /**
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption(self::FORCE_FLAG) === false) {
            $output->writeln('<error>The command has to be run with the force flag!</error>');
            exit(1);
        }

        $this->db->executeStatement('SET FOREIGN_KEY_CHECKS = 0');
        foreach ($this->getObjectTables() as $table) {
            $this->db->executeQuery('TRUNCATE TABLE ' . $table);
        }

        // drop all objects and folders (if set)
        if ($this->input->getOption(self::DROP_FOLDER) === false) {
            $this->db->executeQuery('DELETE FROM objects WHERE id <> 1 and type != "folder"');
        } else {
            $this->db->executeQuery('DELETE FROM objects WHERE id <> 1');
        }

        $this->db->executeQuery('DELETE FROM versions WHERE ctype="object"');
        $this->db->executeQuery('SET FOREIGN_KEY_CHECKS = 1');

        Cache::clearAll();
        $output->writeln(
            PHP_EOL
            . '<comment>If no errors showed up, all object tables have been truncated</comment>'
            . PHP_EOL
        );

        return 0;
    }

    /**
     * @throws Exception
     */
    protected function getObjectTables(): array
    {
        $excludeClassList = $this->input->getOption(self::EXCLUDE_FLAG)
            ? explode(',', $this->input->getOption(self::EXCLUDE_FLAG))
            : [];

        $excludeClassIds = array_map(function ($className) {
            if (!class_exists($className)) {
                throw new InvalidArgumentException("Class <{$className}> not found!");
            }

            return $className::classId();
        }, $excludeClassList);

        $objectTables = array_filter(
            array_map(
                fn ($r) => array_values($r)[0],
                $this->db->fetchAllAssociative("show full tables where Table_Type != 'VIEW';")
            ),
            fn ($item) => (str_starts_with($item, 'object_') && $item !== 'object_url_slugs')
        );

        $truncateTables = [];
        foreach ($objectTables as $tableName) {
            if (!str_starts_with($tableName, 'object_')) {
                continue;
            }
            $parts = explode('_', $tableName);
            $lastPart = (int)array_pop($parts);

            if (in_array($lastPart, $excludeClassIds)) {
                continue;
            }
            $truncateTables[] = $tableName;
        }

        return array_merge($truncateTables, $this->additionalTables);
    }
}
