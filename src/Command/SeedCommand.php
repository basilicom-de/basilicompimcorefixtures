<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Command;

use Basilicom\PimcoreFixtures\Service\Seeder;
use Carbon\Carbon;
use Pimcore\Cache;
use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SeedCommand extends AbstractCommand
{
    protected Seeder $seeder;

    public function __construct(Seeder $seeder, ?string $name = null)
    {
        parent::__construct($name);
        $this->seeder = $seeder;
    }

    protected function configure()
    {
        $this->setName('basilicom:pimcore:fixtures:seed')
            ->setDescription('Seed fixtures by running all registered seeders.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startedAt = Carbon::now();

        $this->seeder
            ->setCommandOutput($output)
            ->run();

        Cache::clearAll();
        $elapsed = Carbon::now()->diffAsCarbonInterval($startedAt)->forHumans() ?: '0s';
        $output->writeln("<comment>Seeding finished after $elapsed.</comment>");

        return 0;
    }
}
