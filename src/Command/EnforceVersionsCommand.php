<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Command;

use Exception;
use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnforceVersionsCommand extends AbstractCommand
{
    public const COMMAND = 'basilicom:pimcore:fixtures:enforce-versions';

    protected array $classNames = [];

    public function __construct(array $classNames, ?string $name = null)
    {
        parent::__construct($name);
        $this->classNames = $classNames;
    }

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND)
            ->setDescription('Saves objects to create versions');
    }

    /**
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!in_array($_ENV['APP_ENV'], ['dev', 'test'])) {
            throw new Exception('this command can only be executed in dev or test environment');
        }

        foreach ($this->classNames as $type) {
            $listingName = 'Pimcore\Model\DataObject\\' . $type . '\Listing';
            /** @var DataObject\Listing $listing */
            $listing = new $listingName();
            $listing->setUnpublished(true);
            foreach ($listing->load() as $dataObject) {
                try {
                    $dataObject->save();
                    $output->writeln(sprintf('saved object #%d of type %s' . PHP_EOL, $dataObject->getId(), $type));
                } catch (Exception) {
                    $output->writeln(sprintf(
                        'could not save object #%d of type %s' . PHP_EOL,
                        $dataObject->getId(),
                        $type
                    ));
                }
            }
        }

        return self::SUCCESS;
    }
}
