<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Command;

use Basilicom\PimcoreFixtures\Service\FixtureLoader;
use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadFixturesCommand extends AbstractCommand
{
    public const COMMAND = 'basilicom:pimcore:fixtures:load';
    protected FixtureLoader $fixtureLoader;

    public function __construct(FixtureLoader $fixtureLoader, string $name = null)
    {
        $this->fixtureLoader = $fixtureLoader;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName(self::COMMAND)
            ->setDescription('Imports yml fixtures')
            ->addArgument(
                'input-folder',
                InputArgument::OPTIONAL,
                'Input folder containing .yml fixtures to load',
                FixtureLoader::DEFAULT_FIXTURE_FOLDER
            )
            ->addOption(
                'omit-validation',
                null,
                InputOption::VALUE_NONE,
                'Omits object validation'
            )
            ->addOption(
                'check-path-exists',
                null,
                InputOption::VALUE_NONE,
                'If set it will check if the path already exists and if yes then it will update the values'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputFolder = $input->getArgument('input-folder');
        $omitValidation = $input->getOption('omit-validation');
        $checkPathExists = $input->getOption('check-path-exists');

        $output->writeln(sprintf('<info>Loading fixtures from: %s</info>', $inputFolder));

        $fixtureFiles = $this->fixtureLoader->getFixturesFiles($inputFolder);

        $this->fixtureLoader->setCheckPathExists($checkPathExists);
        $this->fixtureLoader->setOmitValidation($omitValidation);
        $this->fixtureLoader->load($fixtureFiles);

        $output->writeln('<info>Done!</info>');

        return 0;
    }
}
