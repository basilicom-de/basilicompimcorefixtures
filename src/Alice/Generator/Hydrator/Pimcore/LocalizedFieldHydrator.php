<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\ChainedPropertyHydratorInterface;
use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\ObjectInterface;
use Pimcore\Model\DataObject\ClassDefinition\Data\Localizedfields;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Fieldcollection\Data\AbstractData;
use Pimcore\Tool;
use UnexpectedValueException;

final class LocalizedFieldHydrator implements ChainedPropertyHydratorInterface
{
    use InspectingFieldDefinitionTrait;

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     *
     * @param ObjectInterface   $object
     * @param Property          $property
     * @param GenerationContext $context
     *
     * @return bool
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return $this->isLocalizedField($object->getInstance(), $property->getName());
    }

    /**
     * Hydrate the object with the provided.
     *
     * @param ObjectInterface   $object
     * @param Property          $property
     *
     * @param GenerationContext $context
     *
     * @return ObjectInterface
     */
    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        $instance = $object->getInstance();
        $setter = 'set' . $property->getName();
        $values = $property->getValue();

        if (!is_array($values)) {
            throw new UnexpectedValueException('Expected localized field to contain array of values.');
        }

        foreach (Tool::getValidLanguages() as $systemLanguage) {
            if (array_key_exists($systemLanguage, $values)) {
                $value = $values[$systemLanguage];
                if (is_string($value)) {
                    $value = stripslashes($value);
                }

                $instance->$setter($value, $systemLanguage);
            }
        }

        return $object;
    }

    private function isLocalizedField(object $objectInstance, string $propertyName): bool
    {
        /** @var Concrete $objectInstance */
        if ($objectInstance instanceof Concrete
            || $objectInstance instanceof AbstractData
        ) {
            return $this->getDefinitionForField($objectInstance, $propertyName) instanceof Localizedfields;
        }

        return false;
    }
}
