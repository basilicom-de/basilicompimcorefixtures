<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\ChainedPropertyHydratorInterface;
use Exception;
use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\ObjectInterface;
use Pimcore\Model\DataObject\ClassDefinition\Data\RgbaColor;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Fieldcollection\Data\AbstractData;

final class RgbaColorHydrator implements ChainedPropertyHydratorInterface
{
    use InspectingFieldDefinitionTrait;

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     *
     * @param ObjectInterface   $object
     * @param Property          $property
     * @param GenerationContext $context
     *
     * @return bool
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return $this->isRgbaColor($object->getInstance(), $property->getName());
    }

    /**
     * Hydrate the object with the provided.
     *
     * @param ObjectInterface   $object
     * @param Property          $property
     *
     * @param GenerationContext $context
     *
     * @return ObjectInterface
     * @throws Exception
     */
    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        $instance = $object->getInstance();
        $setter = 'set' . $property->getName();
        $value = $property->getValue();

        if (!is_string($value)) {
            return $object;
        }

        $valueObject = new \Pimcore\Model\DataObject\Data\RgbaColor();
        $valueObject->setHex(str_replace('#', '', $value));

        $instance->$setter($valueObject);

        return $object;
    }

    private function isRgbaColor(object $objectInstance, string $propertyName): bool
    {
        /** @var Concrete $objectInstance */
        if ($objectInstance instanceof Concrete
            || $objectInstance instanceof AbstractData
        ) {
            return $this->getDefinitionForField($objectInstance, $propertyName) instanceof RgbaColor;
        }

        return false;
    }
}
