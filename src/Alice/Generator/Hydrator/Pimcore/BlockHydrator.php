<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\ChainedPropertyHydratorInterface;
use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\ObjectInterface;
use Pimcore\Model\DataObject\ClassDefinition\Data\Block;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Data\BlockElement;
use Pimcore\Model\DataObject\Data\QuantityValue;

final class BlockHydrator implements ChainedPropertyHydratorInterface
{
    use InspectingFieldDefinitionTrait;

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return $this->isBlockField($object->getInstance(), $property->getName());
    }

    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        $blockData = $property->getValue();

        $block = [];
        foreach ($blockData as $blockDataItem) {
            $name = $blockDataItem['name'];
            $type = $blockDataItem['type'] ?? 'input';
            $data = $blockDataItem['data'];

            if ($type === 'manyToOneRelation' || $type === 'select') {
                $data = Concrete::getById($data);
            } elseif ($type === 'quantityValue') {
                $data = new QuantityValue($data['value'], $data['unit']);
            } elseif ($type !== 'input') {
                continue;
            }

            $block[] = [$name => new BlockElement($name, $type, $data)];
        }

        $setter = 'set' . $property->getName();
        $object->getInstance()->$setter($block);

        return $object;
    }

    private function isBlockField(object $objectInstance, string $propertyName): bool
    {
        /** @var Concrete $objectInstance */
        if (false === $objectInstance instanceof Concrete) {
            return false;
        }

        return $this->getDefinitionForField($objectInstance, $propertyName) instanceof Block;
    }
}
