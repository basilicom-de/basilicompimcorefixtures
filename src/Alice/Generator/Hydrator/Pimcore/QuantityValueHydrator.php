<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\ChainedPropertyHydratorInterface;
use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\ObjectInterface;
use Pimcore\Model\DataObject\ClassDefinition\Data\QuantityValue;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Fieldcollection\Data\AbstractData;
use UnexpectedValueException;

final class QuantityValueHydrator implements ChainedPropertyHydratorInterface
{
    use InspectingFieldDefinitionTrait;

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return $this->isQuantityValue($object->getInstance(), $property->getName());
    }

    /**
     * Hydrate the object with the provided.
     */
    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        $instance = $object->getInstance();
        $setter = 'set' . $property->getName();
        $values = (array) $property->getValue();

        if (is_null($property->getValue())) {
            return $object;
        }

        if (!array_key_exists('value', $values) || !array_key_exists('unit', $values)) {
            throw new UnexpectedValueException(
                'Expected quantity value to contain the following keys: value, unit. (@' . $property->getName() . ')'
            );
        }

        $valueObject = new \Pimcore\Model\DataObject\Data\QuantityValue(
            $values['value'],
            $values['unit']
        );

        $instance->$setter($valueObject);

        return $object;
    }

    private function isQuantityValue(object $objectInstance, string $propertyName): bool
    {
        /** @var Concrete $objectInstance */
        if ($objectInstance instanceof Concrete
            || $objectInstance instanceof AbstractData
        ) {
            return $this->getDefinitionForField($objectInstance, $propertyName) instanceof QuantityValue;
        }

        return false;
    }
}
