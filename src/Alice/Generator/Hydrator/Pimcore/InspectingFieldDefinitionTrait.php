<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Pimcore\Model\AbstractModel;
use Pimcore\Model\DataObject\ClassDefinition\Data\Localizedfields;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Fieldcollection\Data\AbstractData;

trait InspectingFieldDefinitionTrait
{
    protected function getDefinitionForField(AbstractModel $objectInstance, string $propertyName)
    {
        if ($objectInstance instanceof AbstractData) {
            $def = $objectInstance->getDefinition();

            $fieldDefinitions = $def->getFieldDefinitions();
            foreach ($fieldDefinitions as $fieldDefinition) {
                if ($fieldDefinition instanceof Localizedfields) {
                    $nestedDefinitions = $fieldDefinition->getFieldDefinitions();
                    foreach ($nestedDefinitions as $localizedKey => $localizedFd) {
                        if ($localizedKey === $propertyName) {
                            return $fieldDefinition;
                        }
                    }
                }
            }
        } elseif ($objectInstance instanceof Concrete) {
            $fieldDefinitions = $objectInstance->getClass()->getFieldDefinitions();

            foreach ($fieldDefinitions as $definitionKey => $fieldDefinition) {
                if ($definitionKey === $propertyName) {
                    return $fieldDefinition;
                }

                if ($fieldDefinition instanceof Localizedfields) {
                    $nestedDefinitions = $fieldDefinition->getFieldDefinitions();
                    foreach ($nestedDefinitions as $localizedKey => $localizedFd) {
                        if ($localizedKey === $propertyName) {
                            return $fieldDefinition;
                        }
                    }
                }
            }
        }

        return null;
    }
}
