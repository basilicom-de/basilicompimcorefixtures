<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Basilicom\PimcoreFixtures\Alice\Data\Pimcore\Relation\TraceableObjectMetadata;
use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\ChainedPropertyHydratorInterface;
use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\ObjectInterface;
use Pimcore\Model\DataObject\ClassDefinition\Data\AdvancedManyToManyObjectRelation;
use Pimcore\Model\DataObject\Concrete;
use RuntimeException;

class AdvancedManyToManyObjectRelationHydrator implements ChainedPropertyHydratorInterface
{
    use InspectingFieldDefinitionTrait;

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return $this->isAdvancedManyToManyObjectField($object->getInstance(), $property->getName());
    }

    private function isAdvancedManyToManyObjectField(object $objectInstance, string $propertyName): bool
    {
        /** @var Concrete $objectInstance */
        if (false === $objectInstance instanceof Concrete) {
            return false;
        }

        return $this->getDefinitionForField($objectInstance, $propertyName) instanceof AdvancedManyToManyObjectRelation;
    }

    /**
     * Hydrate the object with the provided.
     */
    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        $relations = [];

        foreach ((array) $property->getValue() as $value) {
            $relatedObject = $value['__key'];
            unset($value['__key']);

            if ($relatedObject instanceof Concrete === false) {
                throw new RuntimeException('Expected a Pimcore object for __key to be set!');
            }

            $columns = array_keys($value);

            $objectMetaData = new TraceableObjectMetadata($property->getName(), $columns, $relatedObject);
            foreach ($columns as $columnKey) {
                $objectMetaData->setValue($columnKey, $value[$columnKey]);
            }
            $relations[] = $objectMetaData;
        }

        // needs preprocessing before save due to related obj id...
        $object->getInstance()->__relations[$property->getName()] = $relations;
        $setter = 'set' . $property->getName();
        $object->getInstance()->$setter(null);

        return $object;
    }
}
