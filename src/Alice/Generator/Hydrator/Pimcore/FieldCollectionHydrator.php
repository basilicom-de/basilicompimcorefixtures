<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore;

use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\ChainedPropertyHydratorInterface;
use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\ObjectInterface;
use Pimcore\Model\DataObject\ClassDefinition\Data\Fieldcollections;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Fieldcollection;

final class FieldCollectionHydrator implements ChainedPropertyHydratorInterface
{
    use InspectingFieldDefinitionTrait;

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return $this->isFieldCollectionField($object->getInstance(), $property->getName());
    }

    /**
     * Hydrate the object with the provided.
     */
    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        $collectionItems = $property->getValue();
        $fieldCollection = new Fieldcollection($collectionItems);

        // todo => get items and hydrate based on items field definitions....

        $setter = 'set' . $property->getName();
        $object->getInstance()->$setter($fieldCollection);

        return $object;
    }

    private function isFieldCollectionField(object $objectInstance, string $propertyName): bool
    {
        /** @var Concrete $objectInstance */
        if (false === $objectInstance instanceof Concrete) {
            return false;
        }

        return $this->getDefinitionForField($objectInstance, $propertyName) instanceof Fieldcollections;
    }
}
