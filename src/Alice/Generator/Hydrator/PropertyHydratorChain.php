<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator;

use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\Generator\Hydrator\PropertyHydratorInterface;
use Nelmio\Alice\ObjectInterface;
use RuntimeException;

class PropertyHydratorChain implements PropertyHydratorInterface
{
    /** @var ChainedPropertyHydratorInterface[] */
    private array $hydrators = [];

    public function addHydrator(ChainedPropertyHydratorInterface $propertyHydrator): void
    {
        $this->hydrators[] = $propertyHydrator;
    }

    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        foreach ($this->hydrators as $hydrator) {
            if ($hydrator->canHydrate($object, $property, $context)) {
                return $hydrator->hydrate($object, $property, $context);
            }
        }

        throw new RuntimeException(
            sprintf(
                'No hydrator took responsibility for: "%s" on "%s"',
                $property->getName(),
                $object->getId()
            )
        );
    }
}
