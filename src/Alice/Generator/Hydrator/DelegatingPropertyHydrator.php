<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator;

use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\Generator\Hydrator\PropertyHydratorInterface;
use Nelmio\Alice\ObjectInterface;
use Nelmio\Alice\PropertyAccess\ReflectionPropertyAccessor;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Nelmio\Alice\Generator\Hydrator\Property\SymfonyPropertyAccessorHydrator;

final class DelegatingPropertyHydrator implements ChainedPropertyHydratorInterface
{
    private ?PropertyHydratorInterface $delegate = null;

    private array $ignoredProperties = [
        'hasChildren',
        'children',
        'childrenSortBy',
        'versionCount',
        '__dataVersionTimestamp',
    ];

    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool
    {
        return true;
    }

    /**
     * Hydrate the object with the provided.
     */
    public function hydrate(ObjectInterface $object, Property $property, GenerationContext $context): ObjectInterface
    {
        // Ignored props.
        if (in_array($property->getName(), $this->ignoredProperties)) {
            return $object;
        }

        // Unescape strings
        if (is_string($property->getValue())) {
            $property = $property->withValue(
                stripslashes($property->getValue())
            );
        }

        return $this->getDelegate()->hydrate($object, $property, $context);
    }

    private function getDelegate(): PropertyHydratorInterface
    {
        if (is_null($this->delegate)) {
            $this->delegate = new SymfonyPropertyAccessorHydrator(
                new ReflectionPropertyAccessor(
                    new PropertyAccessor()
                )
            );
        }

        return $this->delegate;
    }
}
