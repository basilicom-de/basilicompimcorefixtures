<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Generator\Hydrator;

use Nelmio\Alice\Definition\Property;
use Nelmio\Alice\Generator\GenerationContext;
use Nelmio\Alice\Generator\Hydrator\PropertyHydratorInterface;
use Nelmio\Alice\ObjectInterface;

interface ChainedPropertyHydratorInterface extends PropertyHydratorInterface
{
    /**
     * Whether this Hydrator can handle the current object's property.
     * If true, hydrate will be called and the chain execution is stopped here.
     */
    public function canHydrate(ObjectInterface $object, Property $property, GenerationContext $context): bool;
}
