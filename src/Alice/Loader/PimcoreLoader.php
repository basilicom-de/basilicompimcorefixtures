<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Loader;

use Faker\Generator as FakerGenerator;
use Nelmio\Alice\Generator\Hydrator\PropertyHydratorInterface;
use Nelmio\Alice\Loader\NativeLoader;

class PimcoreLoader extends NativeLoader
{
    private PropertyHydratorInterface $propertyHydratorChain;

    public function __construct(
        PropertyHydratorInterface $propertyHydratorChain,
        FakerGenerator $fakerGenerator = null
    ) {
        $this->propertyHydratorChain = $propertyHydratorChain;

        parent::__construct($fakerGenerator);
    }

    protected function createPropertyHydrator(): PropertyHydratorInterface
    {
        return $this->propertyHydratorChain;
    }
}
