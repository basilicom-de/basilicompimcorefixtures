<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Loader;

use Basilicom\PimcoreFixtures\Alice\Persistence\PersisterInterface;
use Basilicom\PimcoreFixtures\Alice\Processor\ProcessorInterface;
use Nelmio\Alice\FilesLoaderInterface;
use Nelmio\Alice\ObjectSet;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class PersisterLoader
{
    /** @var FilesLoaderInterface */
    protected FilesLoaderInterface $loader;

    /** @var PersisterInterface */
    protected PersisterInterface $persister;

    /** @var LoggerInterface */
    protected LoggerInterface $logger;

    /** @var ProcessorInterface[] */
    protected array $processors;

    public function __construct(
        FilesLoaderInterface $loader,
        PersisterInterface $persister,
        LoggerInterface $logger = null,
        array $processors = []
    ) {
        $this->loader = $loader;
        $this->persister = $persister;
        $this->logger = $logger ?? new NullLogger();
        $this->processors = $processors;
    }

    /**
     * Preprocess, persist and post process each object loaded.
     */
    public function load(array $fixturesFiles, array $parameters = [], array $objects = []): array
    {
        $fileSort = [];
        foreach ($fixturesFiles as $filepath) {
            $pathParts = explode('/', $filepath);
            $filename = array_pop($pathParts);
            $key = (int) substr($filename, 0, 3);

            $filenameParts = explode('_', $filename);
            $fixtureName = count($filenameParts) > 1 ? $filenameParts[1] : $filenameParts[0];

            $typename = str_replace('.yml', '', $fixtureName);
            $fileSort[$typename] = $key;
        }

        $objectSet = $this->loader->loadFiles($fixturesFiles, $parameters, $objects);

        $this->logger->info('Pre-processing objects.');

        $objects = $this->getSortedObjects($objectSet, $fileSort);

        foreach ($objects as $id => $object) {
            foreach ($this->processors as $processor) {
                $processor->preProcess($id, $object);
            }

            $this->persister->persist($object);

            // TODO: flush for each level only!
            $this->persister->flush();

            foreach ($this->processors as $processor) {
                $processor->postProcess($id, $object);
            }
        }

        $this->logger->info('Done.');

        return $objects;
    }

    /**
     * TODO: custom logic out of package.
     */
    private function getSortedObjects(ObjectSet $objectSet, array $fileSortOrderMap): array
    {
        $objects = $objectSet->getObjects();
        $objectKeys = array_keys($objects);

        $sortByType = array_map(
            function ($key) use ($fileSortOrderMap) {
                $typenamePart = explode('_', $key)[1];

                return $fileSortOrderMap[$typenamePart];
            },
            $objectKeys
        );

        $sortByDepth = array_map(
            function ($key) {
                return (int) explode('_', $key)[0];
            },
            $objectKeys
        );

        array_multisort(
            $sortByType,
            SORT_ASC,
            $sortByDepth,
            SORT_ASC,
            $objectKeys,
            SORT_ASC,
            $objects
        );

        return $objects;
    }
}
