<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Data\Pimcore\Relation;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Data\ObjectMetadata;

/**
 * An extension of the original relation that keeps track of the object instance,
 * not only the object id (which might not be present at instantiation time yet)
 */
class TraceableObjectMetadata extends ObjectMetadata
{
    protected ?DataObject\Concrete $tracedObject;

    public function setObject(?DataObject\Concrete $object): static
    {
        $this->tracedObject = $object;
        parent::setObject($object);

        return $this;
    }

    public function getObject(): ?DataObject\Concrete
    {
        if ($this->tracedObject) {
            return $this->tracedObject;
        }

        return parent::getObject();
    }

    public function __sleep(): array
    {
        $this->tracedObject = null;

        return parent::__sleep();
    }
}
