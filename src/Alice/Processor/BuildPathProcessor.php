<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Processor;

use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Folder;
use Pimcore\Model\DataObject\Service;

class BuildPathProcessor implements ProcessorInterface
{
    /**
     * Processes an object before it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function preProcess(string $id, $object): void
    {
        if ($object instanceof Concrete
            || $object instanceof Folder
        ) {
            $path = $this->buildPathRecursive($object);
            $object->setPath($path);

            $parent = $this->findExistingParent($object);
            if ($parent) {
                $object->setParentId($parent->getId());
                $object->setParent($parent);
            }
        }
    }

    /**
     * Processes an object after it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function postProcess(string $id, $object): void
    {
    }

    private function buildPathRecursive(AbstractObject $element, array $keys = []): string
    {
        if ($element->getId() === 1 || $element->getParentId() === 0) {
            return implode('/', array_reverse($keys)) . '/';
        }

        $parent = $element->getParent();
        $keys[] = $parent->getKey();

        return $this->buildPathRecursive($parent, $keys);
    }

    private function findExistingParent(AbstractObject $object): Folder|Concrete|null
    {
        // Update if object of same class and path exists.
        if (Service::pathExists($object->getPath())) {
            if ($object instanceof Concrete) {
                return Concrete::getByPath($object->getPath());
            } elseif ($object instanceof Folder) {
                return Folder::getByPath($object->getPath());
            }
        }

        // TODO: problem!
        return null;
    }
}
