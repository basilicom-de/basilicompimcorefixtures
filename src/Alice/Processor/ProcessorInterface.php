<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Processor;

interface ProcessorInterface
{
    /**
     * Processes an object before it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function preProcess(string $id, $object): void;

    /**
     * Processes an object after it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function postProcess(string $id, $object): void;
}
