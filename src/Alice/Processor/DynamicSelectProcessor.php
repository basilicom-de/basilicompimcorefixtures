<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Processor;

use Exception;
use Pimcore\Model\DataObject\ClassDefinition\Data\Block;
use Pimcore\Model\DataObject\ClassDefinition\Data\Fieldcollections;
use Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect;
use Pimcore\Model\DataObject\ClassDefinition\Data\Select;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Data\BlockElement;
use Pimcore\Model\DataObject\Fieldcollection;

class DynamicSelectProcessor implements ProcessorInterface
{
    /**
     * Processes an object before it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     *
     * @throws Exception
     */
    public function preProcess(string $id, $object): void
    {
        if ($object instanceof Concrete === false) {
            return;
        }

        $this->processFields($object->getClass()->getFieldDefinitions(), $object);
    }

    private function processFields($definition, $context): void
    {
        foreach ($definition as $key => $def) {
            if ($def instanceof Fieldcollections) {
                $fieldCollection = $context->{'get' . ucfirst($key)}();
                if ($fieldCollection instanceof Fieldcollection) {
                    foreach ($fieldCollection->getItems() as $item) {
                        $collectionName = array_keys($fieldCollection->getItemDefinitions())[0];
                        $fcDef = $fieldCollection->getItemDefinitions()[$collectionName];
                        $this->processFields($fcDef->getFieldDefinitions(), $item);
                    }
                }
            } elseif ($def instanceof Multiselect || $def instanceof Select) {
                $this->preProcessFieldDefinition($key, $context);
            } elseif ($def instanceof Block) {
                $this->preProcessBlock($key, $context);
            }
        }
    }

    private function preProcessFieldDefinition($field, $object): void
    {
        $getter = 'get' . ucfirst($field);
        $setter = 'set' . ucfirst($field);

        $objectValue = $object->$getter();
        $processedValue = null;

        if (is_array($objectValue)) {
            foreach ($objectValue as $val) {
                if ($val instanceof Concrete) {
                    $processedValue[] = $val->getId();
                }
            }
        } elseif ($objectValue instanceof Concrete) {
            $processedValue = $objectValue->getId();
        }

        if ($processedValue !== null) {
            $object->$setter($processedValue);
        }
    }

    /**
     * Processes an object after it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function postProcess(string $id, $object): void
    {
        // nothing.
    }

    /**
     * todo =>  we wouldnt need this if hydrating would work as expected or at least allow deep-hydrating of FieldCollections
     *          hydrators would take the fixture data and create object instances, etc. and set it
     *
     * @param $key
     * @param $context
     */
    private function preProcessBlock($key, $context): void
    {
        $getter = 'get' . ucfirst($key);
        $setter = 'set' . ucfirst($key);

        /** @var BlockElement[][] $blockElements */
        $blockElements = $context->$getter();

        $processedValue = [];
        foreach ($blockElements as $blockElement) {
            $block = [];
            foreach ($blockElement as $blockElementData) {
                if ($blockElementData instanceof BlockElement) {
                    $name = $blockElementData->getName();
                    $block[$name] = $blockElementData;
                } else {
                    $name = $blockElementData['name'];
                    $type = $blockElementData['type'];
                    $data = $blockElementData['data'];

                    // todo => add additional types where we need to map the dataObject to only the id/path
                    if ($type === 'select' && $data instanceof Concrete) {
                        $data = $data->getId();
                    }

                    $block[$name] = new BlockElement($name, $type, $data);
                }
            }

            $processedValue[] = $block;
        }

        if (!empty($processedValue)) {
            $context->$setter($processedValue);
        }
    }
}
