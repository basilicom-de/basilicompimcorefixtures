<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Processor;

use Basilicom\PimcoreFixtures\Alice\Data\Pimcore\Relation\TraceableObjectMetadata;
use Exception;
use Pimcore\Model\DataObject\ClassDefinition\Data\AdvancedManyToManyObjectRelation;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Data\ObjectMetadata;

class ObjectMetadataProcessor implements ProcessorInterface
{
    /**
     * Processes an object before it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     *
     * @throws Exception
     */
    public function preProcess(string $id, $object): void
    {
        if ($object instanceof Concrete === false) {
            return;
        }

        $this->processFields($object->getClass()->getFieldDefinitions(), $object);
    }

    private function processFields($definition, $context): void
    {
        foreach ($definition as $key => $def) {
            if ($def instanceof AdvancedManyToManyObjectRelation) {
                if (!is_array($context->__relations[$key])) {
                    continue;
                }

                $relations = [];
                /** @var TraceableObjectMetadata $traceableMeta */
                foreach ($context->__relations[$key] as $traceableMeta) {
                    if ($obj = $traceableMeta->getObject()) {
                        $traceableMeta->setObjectId($obj->getId());
                    }

                    $metadata = new ObjectMetadata($key, $traceableMeta->getColumns(), $traceableMeta->getObject());
                    $metadata->setData($traceableMeta->getData());

                    $relations[] = $metadata;
                }

                $context->{'set' . $key}($relations);

                unset($context->__relations[$key]);
            }
        }
    }

    /**
     * Processes an object after it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function postProcess(string $id, $object): void
    {
    }
}
