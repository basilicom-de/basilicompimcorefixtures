<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Processor;

use Pimcore\Model\DataObject\Folder;

class FolderProcessor implements ProcessorInterface
{
    /**
     * Processes an object before it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function preProcess(string $id, $object): void
    {
        if ($object instanceof Folder === false) {
            return;
        }

        $existingFolder = Folder::getByPath($object->getFullPath());
        if ($existingFolder) {
            $existingProps = $existingFolder->getProperties();
            $currentProps = $object->getProperties();

            $object->setProperties(
                array_merge($existingProps, $currentProps)
            );
        }
    }

    /**
     * Processes an object after it is persisted to DB.
     *
     * @param string $id Fixture ID
     * @param object $object
     */
    public function postProcess(string $id, $object): void
    {
        // nothing.
    }
}
