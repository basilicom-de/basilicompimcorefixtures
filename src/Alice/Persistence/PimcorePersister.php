<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Persistence;

use Exception;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Document;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Objectbrick;
use UnexpectedValueException;

class PimcorePersister implements PersisterInterface
{
    private bool $checkPathExists;
    private bool $omitValidation;
    private array $objectsToPersist;

    public function __construct(bool $checkPathExists, bool $omitValidation)
    {
        $this->checkPathExists = $checkPathExists;
        $this->omitValidation = $omitValidation;
    }

    public function persist($object): void
    {
        $this->objectsToPersist[] = $object;
    }

    /**
     * @throws Exception
     */
    public function flush(): void
    {
        foreach ($this->objectsToPersist as $object) {
            switch (true) {
                case $object instanceof Asset:
                case $object instanceof DataObject:
                    $this->persistObject($object);
                    break;
                case $object instanceof Objectbrick:
                    $this->persistObjectBrick($object);
                    break;
                default:
                    break;
            }
        }

        $this->objectsToPersist = [];
    }

    /**
     * @throws Exception
     */
    private function persistObject(Asset|Document|DataObject $element): void
    {
        if ($this->checkPathExists === true) {
            // Update if object of same class and path exists.
            $this->ensureUpdateIfElementExists($element);
        }

        // We expect an element, only Object\Concrete has mandatory fields
        if (method_exists($element, 'setOmitMandatoryCheck')) {
            $element->setOmitMandatoryCheck($this->omitValidation);
        }

        $element->setParentId($element->getParent()->getId());
        $element->save();
    }

    /**
     * @param Objectbrick $objectBrick
     *
     * @throws Exception
     */
    private function persistObjectBrick(Objectbrick $objectBrick): void
    {
        $setter = 'set' . $objectBrick->getFieldname();

        $ownerObject = $objectBrick->getObject();

        if ($this->checkPathExists === true) {
            $this->ensureUpdateIfElementExists($ownerObject);
        }

        if (!method_exists($ownerObject, $setter)) {
            throw new UnexpectedValueException(
                sprintf('Object with class %s has no setter %s', get_class($objectBrick), $setter)
            );
        }

        $ownerObject->$setter($objectBrick);
        $ownerObject->save();
    }

    private function ensureUpdateIfElementExists($element): void
    {
        if (DataObject\Service::pathExists($element->getRealFullPath())) {
            $identity = $element::getByPath($element->getRealFullPath());

            $elementClass = get_class($element);
            if ($identity instanceof $elementClass) {
                $element->setId($identity->getId());
            } elseif ($identity) {
                $identity->delete();
            }
        }
    }
}
