<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Persistence;

interface PersisterInterface
{
    public function persist($object): void;

    public function flush(): void;
}
