<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Alice\Factory;

use Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\PropertyHydratorChain;
use Basilicom\PimcoreFixtures\Alice\Loader\PersisterLoader;
use Basilicom\PimcoreFixtures\Alice\Persistence\PimcorePersister;
use Basilicom\PimcoreFixtures\Alice\Processor\BuildPathProcessor;
use Basilicom\PimcoreFixtures\Alice\Processor\DynamicSelectProcessor;
use Basilicom\PimcoreFixtures\Alice\Processor\FolderProcessor;
use Basilicom\PimcoreFixtures\Alice\Loader\PimcoreLoader;
use Basilicom\PimcoreFixtures\Alice\Processor\ObjectMetadataProcessor;
use Nelmio\Alice\Loader\SimpleFilesLoader;
use Nelmio\Alice\Parser\Chainable\YamlParser;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Yaml\Parser;

class PersisterLoaderFactory
{
    protected PropertyHydratorChain $propertyHydratorChain;
    protected LoggerInterface $logger;

    public function __construct(
        PropertyHydratorChain $propertyHydratorChain,
        LoggerInterface $logger = null
    ) {
        $this->propertyHydratorChain = $propertyHydratorChain;
        $this->logger = $logger ?? new NullLogger();
    }

    public function createPersisterLoader(
        bool $checkPathExists,
        bool $omitValidation
    ): PersisterLoader {
        $decoratedLoader = new SimpleFilesLoader(
            new YamlParser(new Parser()),
            new PimcoreLoader($this->propertyHydratorChain)
        );

        return new PersisterLoader(
            $decoratedLoader,
            new PimcorePersister($checkPathExists, $omitValidation),
            $this->logger,
            [
                new BuildPathProcessor(),
                new FolderProcessor(),
                new DynamicSelectProcessor(),
                new ObjectMetadataProcessor(),
            ]
        );
    }
}
