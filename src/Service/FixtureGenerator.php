<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Service;

use Basilicom\PimcoreFixtures\Generation\FileSort\FileSortInterface;
use Basilicom\PimcoreFixtures\Generation\FileSort\SortByKey;
use Basilicom\PimcoreFixtures\Generation\ObjectValueExtractor;
use Basilicom\PimcoreFixtures\Generation\ValueExtractorInterface;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\Folder;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Yaml\Yaml;

class FixtureGenerator
{
    public const ALL_OBJ_TYPES = [
        AbstractObject::OBJECT_TYPE_OBJECT,
        AbstractObject::OBJECT_TYPE_FOLDER,
        AbstractObject::OBJECT_TYPE_VARIANT,
    ];

    private AbstractObject|Folder $folder;
    private int $maxLevels;
    private ValueExtractorInterface $valueExtractorChain;
    private ?FileSortInterface $fileSorter = null;

    public function setFolder(Folder $folder): void
    {
        $this->folder = $folder;
    }

    public function setMaxLevels(int $maxLevels): void
    {
        $this->maxLevels = $maxLevels;
    }

    public function __construct(ValueExtractorInterface $valueExtractorChain)
    {
        $this->valueExtractorChain = $valueExtractorChain;
    }

    /**
     * Gets all children from specified folder and outputs to yml the result
     *
     * @throws ReflectionException
     */
    public function generateFixturesForFolder(): void
    {
        $fixtures = $this->getChildrenRecursive($this->folder);

        $fileSorter = $this->getFileSorter();
        $fixtures = $fileSorter->sort($fixtures);

        foreach ($fixtures as $level => $fixtureClasses) {
            foreach ($fixtureClasses as $class => $fixtureData) {
                $sortPrefix = $fileSorter->getFilePrefix($class);

                $this->writeToFile($fixtureData, $class, $level, $sortPrefix);
            }
        }
    }

    /**
     * @param FileSortInterface $fileSorter
     */
    public function setFileSorter(FileSortInterface $fileSorter): void
    {
        $this->fileSorter = $fileSorter;
    }

    public function getFileSorter(): FileSortInterface
    {
        if (is_null($this->fileSorter)) {
            $this->fileSorter = new SortByKey();
        }

        return $this->fileSorter;
    }

    /**
     * @throws ReflectionException
     */
    private function getChildrenRecursive(AbstractObject $root, array &$fixtures = []): array
    {
        AbstractObject::setHideUnpublished(false);

        foreach ($root->getChildren(self::ALL_OBJ_TYPES, true) as $child) {
            $currentLevel = ObjectValueExtractor::getCurrentLevel($child);
            $valueExtractor = new ObjectValueExtractor($child, $this->valueExtractorChain);

            $values = $valueExtractor->getDataForObject();

            $objKey = ObjectValueExtractor::getUniqueKey($child);
            $shortName = (new ReflectionClass($child))->getShortName();

            $fixtures[$currentLevel][$shortName][get_class($child)][$objKey] = $values;
            if ($valueExtractor->hasObjectBrick()) {
                $valueExtractor->addObjectBricksForObject($child, $fixtures[$currentLevel][$shortName]);
            }

            if ($valueExtractor->hasFieldCollections()) {
                $valueExtractor->addFieldCollectionsForObject($child, $fixtures[$currentLevel][$shortName]);
            }

            if ($valueExtractor->hasPimcoreProperties()) {
                $valueExtractor->addPimcorePropertiesForObject($child, $fixtures[$currentLevel][$shortName]);
            }

            if ($child->getChildren(self::ALL_OBJ_TYPES, true) && ($currentLevel < $this->maxLevels)) {
                $this->getChildrenRecursive($child, $fixtures);
            }
        }

        // Include root folder in fixtures if first node is not Pimcore root object.
        if ($root->getId() > 1 && $root->getId() === $this->folder->getId()) {
            /** @var AbstractObject $child */
            $currentLevel = ObjectValueExtractor::getCurrentLevel($root);
            $objKey = ObjectValueExtractor::getUniqueKey($root);
            $shortName = (new ReflectionClass($root))->getShortName();
            $fixtures[$currentLevel][$shortName][get_class($root)][$objKey] = [
                'parent_id' => 1,
                'key'       => $root->getKey(),
            ];
        }

        return $fixtures;
    }

    private function writeToFile(array $data, string $class, int $level, string $prefix): void
    {
        $yaml = Yaml::dump($data, 3);

        $fixturesFolder = FixtureLoader::DEFAULT_FIXTURE_FOLDER . '_generated' . DIRECTORY_SEPARATOR;

        if (!is_dir($fixturesFolder)) {
            mkdir($fixturesFolder, 0777, true);
        }

        $class = strtolower($class);

        if (!empty($prefix)) {
            $filename = $prefix . '_' . $class;
        } else {
            $filename = $class;
        }

        if ($this->maxLevels > 1
            && file_exists($fixturesFolder . $filename . '.yml')
        ) {
            $filename .= '_' . $level;
        }

        $fullPath = $fixturesFolder . $filename . '.yml';
        file_put_contents($fullPath, $yaml);
    }
}
