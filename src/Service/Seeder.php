<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Service;

use Symfony\Component\Console\Output\OutputInterface;

class Seeder
{
    private FixtureLoader $fixtureLoader;
    private OutputInterface $commandOutput;

    public function __construct(FixtureLoader $fixtureLoader)
    {
        $this->fixtureLoader = $fixtureLoader;
    }

    public function setCommandOutput(OutputInterface $commandOutput): Seeder
    {
        $this->commandOutput = $commandOutput;

        return $this;
    }

    public function run(): void
    {
        $fixtureFiles = $this->fixtureLoader->getFixturesFiles(PIMCORE_PROJECT_ROOT . '/fixtures');

        if (isset($this->commandOutput)) {
            $this->commandOutput->writeln('<info>Start seeding:</info>');
        }

        $this->fixtureLoader->setOmitValidation(true);
        $this->fixtureLoader->setCheckPathExists(true);
        $this->fixtureLoader->load($fixtureFiles);
    }
}
