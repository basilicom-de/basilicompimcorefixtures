<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Service;

use Basilicom\PimcoreFixtures\Alice\Factory\PersisterLoaderFactory;
use InvalidArgumentException;
use Pimcore\Model\Version;

class FixtureLoader
{
    public const DEFAULT_FIXTURE_FOLDER = PIMCORE_PRIVATE_VAR . '/bundles/PimcoreFixtures/fixtures';
    public const DEFAULT_ASSETS_FOLDER = PIMCORE_PRIVATE_VAR . '/bundles/PimcoreFixtures/assets';

    private PersisterLoaderFactory $loaderFactory;
    private bool $omitValidation;
    private bool $checkPathExists;

    public function __construct(
        PersisterLoaderFactory $loaderFactory,
        bool $checkPathExists = true,
        bool $omitValidation = false
    ) {
        $this->loaderFactory = $loaderFactory;

        $this->omitValidation = $omitValidation;
        $this->checkPathExists = $checkPathExists;

        self::createFolderDependencies();
    }

    public function setCheckPathExists(bool $doCheck): void
    {
        $this->checkPathExists = $doCheck;
    }

    public function setOmitValidation(bool $doOmitValidation): void
    {
        $this->omitValidation = $doOmitValidation;
    }

    public function getFixturesFiles(string $inputFolder): array
    {
        if (!is_dir($inputFolder)) {
            throw new InvalidArgumentException('Not a folder: ' . $inputFolder);
        }

        $fixturesFiles = glob(
            $inputFolder . '/*.{yml}',
            GLOB_BRACE
        );

        usort(
            $fixturesFiles,
            function ($a, $b) {
                return strnatcasecmp($a, $b);
            }
        );

        return $fixturesFiles;
    }

    public function load(array $fixtureFiles): void
    {
        Version::disable();
        $this->loaderFactory->createPersisterLoader($this->checkPathExists, $this->omitValidation)->load($fixtureFiles);
    }

    /**
     * Makes sure all folders are created so glob does not throw any error
     */
    private static function createFolderDependencies(): void
    {
        $folders = [
            self::DEFAULT_FIXTURE_FOLDER,
            self::DEFAULT_ASSETS_FOLDER,
        ];

        foreach ($folders as $folder) {
            if (!is_dir($folder)) {
                mkdir($folder, 0777, true);
            }
        }
    }
}
