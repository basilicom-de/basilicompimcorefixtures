<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation;

use Basilicom\PimcoreFixtures\Exception\NotImplementedException;

class ValueExtractorChain implements ValueExtractorInterface
{
    public const KEY_ALL = '__ALL__';

    protected array $extractors = [];

    public function addExtractor(ValueExtractorInterface $extractor, array $fields = [])
    {
        if (empty($fields)) {
            $fields = [self::KEY_ALL];
        }

        foreach ($fields as $field) {
            $this->extractors[$field] = $extractor;
        }
    }

    public function getValue($object, string $propertyName, $fieldDefinition)
    {
        $extractor = $this->getExtractorForField($propertyName);

        return $extractor->getValue($object, $propertyName, $fieldDefinition);
    }

    private function getExtractorForField(string $fieldName): ValueExtractorInterface
    {
        if (array_key_exists($fieldName, $this->extractors)) {
            return $this->extractors[$fieldName];
        }

        if (array_key_exists(self::KEY_ALL, $this->extractors)) {
            return $this->extractors[self::KEY_ALL];
        }

        throw new NotImplementedException(
            sprintf('I have no value extractor for field: %s', $fieldName)
        );
    }
}
