<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation\FileSort;

interface FileSortInterface
{
    public function sort(array $fixtures): array;

    public function getFilePrefix(string $class): string;
}
