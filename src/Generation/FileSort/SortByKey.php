<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation\FileSort;

class SortByKey implements FileSortInterface
{
    public function sort(array $fixtures): array
    {
        ksort($fixtures);

        return $fixtures;
    }

    public function getFilePrefix(string $class): string
    {
        return '';
    }
}
