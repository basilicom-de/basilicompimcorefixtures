<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation;

interface ValueExtractorInterface
{
    public function getValue($object, string $propertyName, $fieldDefinition);
}
