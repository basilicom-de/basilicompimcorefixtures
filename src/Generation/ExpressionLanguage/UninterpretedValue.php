<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation\ExpressionLanguage;

class UninterpretedValue
{
    protected $value;

    public function __construct($value = null)
    {
        $this->value = $value;
    }

    public function get()
    {
        if (empty($this->value) || !is_string($this->value)) {
            return $this->value;
        }

        $pattern = '/(\<|@|\[|\$)/';
        $replacement = '\\\${0}';

        return preg_replace($pattern, $replacement, $this->value) ?? $this->value;
    }
}
