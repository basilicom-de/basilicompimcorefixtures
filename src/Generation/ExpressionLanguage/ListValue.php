<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation\ExpressionLanguage;

class ListValue
{
    protected ?string $value;

    public function __construct(?string $value)
    {
        $this->value = $value;
    }

    public function get(): string
    {
        if (empty($this->value)) {
            return '[]';
        }

        return sprintf('[%s]', $this->value);
    }
}
