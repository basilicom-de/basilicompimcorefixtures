<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation\ExpressionLanguage;

use Carbon\Carbon;

class DateTimeValue
{
    protected Carbon|int|string|null $value;

    public function __construct(Carbon|int|string|null $value)
    {
        $this->value = $value;
    }

    public function get(): ?string
    {
        if ($this->value instanceof Carbon) {
            $this->value = $this->value->toIso8601String();
        } elseif (!is_numeric($this->value)) {
            return null;
        }

        return sprintf('<(\Carbon\Carbon::parse("%s"))>', $this->value);
    }
}
