<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Generation;

use Basilicom\PimcoreFixtures\Exception\NotImplementedException;
use Basilicom\PimcoreFixtures\Generation\ExpressionLanguage as Expr;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\Data\Block;
use Pimcore\Model\DataObject\ClassDefinition\Data\Fieldcollections;
use Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyObjectRelation;
use Pimcore\Model\DataObject\ClassDefinition\Data\Objectbricks;
use Pimcore\Model\DataObject\ClassDefinition\Data\Relations\AbstractRelations;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Data\BlockElement;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\DataObject\Fieldcollection\Data\AbstractData;
use Pimcore\Model\Property;
use Pimcore\Tool;
use ReflectionClass;

class ObjectValueExtractor
{
    private ?AbstractObject $object;

    private static array $ignoredFields = [
        '_fulldump',
        'dirtyFields',
        'classId',
        'className',
        'class',
        'versions',
        'lazyLoadedFields',
        'scheduledTasks',
        'omitMandatoryCheck',
        'id',
        'parentId',
        'parent',
        'type',
        'path',
        'index',
        'creationDate',
        'modificationDate',
        'userOwner',
        'userModification',
        'properties',
        'hasChilds', // compat only
        'siblings',
        'hasSiblings',
        'dependencies',
        'childs',
        'locked',
        'elementAdminStyle',
        '____pimcore_cache_item__',
        'key',
        'published',
        'hasChildren',
        'children',
        'childrenSortBy',
        'childrenSortOrder',
        'versionCount',
        '__dataVersionTimestamp',
        'dao',
        'cid',
        'cpath',
        'inherited',
    ];

    private ValueExtractorInterface $valueExtractorChain;

    /**
     * // TODO
     *  - check all relation types
     *  - check enum relations (select)
     */
    public function __construct(AbstractObject $object, ValueExtractorInterface $valueExtractorChain)
    {
        $this->object = $object;
        $this->valueExtractorChain = $valueExtractorChain;
    }

    public function getDataForObject(): array
    {
        if ($this->object instanceof Concrete) {
            $values = [];
            foreach ($this->object->getClass()->getFieldDefinitions() as $key => $def) {
                try {
                    $values[$key] = $this->valueExtractorChain->getValue($this->object, $key, $def);
                } catch (NotImplementedException) {
                    // Known fields that we don`t need their values
                    if (!($def instanceof DataObject\ClassDefinition\Data\CalculatedValue
                        || $def instanceof DataObject\ClassDefinition\Data\Classificationstore
                        || $def instanceof DataObject\ClassDefinition\Data\Fieldcollections
                        || $def instanceof Objectbricks)
                    ) {
                        if ($def instanceof DataObject\ClassDefinition\Data\Localizedfields) {
                            foreach ($def->getFieldDefinitions() as $localizedKey => $localizedFd) {
                                $getter = 'get' . ucfirst($localizedKey);
                                $systemLanguages = Tool::getValidLanguages();
                                foreach ($systemLanguages as $language) {
                                    $values[$localizedKey][$language] =
                                        (new Expr\UninterpretedValue($this->object->$getter($language)))->get();
                                }
                            }
                        } else {
                            $values[$key] = $this->getDataForField($this->object, $key, $def);
                        }
                    }
                }
            }
        } else {
            $values = $this->filterVars($this->object);
        }

        $this->addSystemReferences($values);

        return $values;
    }

    private function getDataForField(Concrete $object, string $key, Data $fieldDefinition)
    {
        if ($fieldDefinition instanceof AbstractRelations) {
            $method = 'get' . ucfirst($key);
            $objectValue = $object->$method();

            $value = [];
            if (!is_null($objectValue)) {
                if ($fieldDefinition instanceof DataObject\ClassDefinition\Data\ManyToOneRelation) {
                    $relatedObj = AbstractObject::getById($objectValue->getId());
                    if (!is_null($relatedObj)) {
                        $objKey = $this->getUniqueKey($relatedObj);
                        $value = '@' . $objKey;
                    }
                } elseif ($fieldDefinition instanceof DataObject\ClassDefinition\Data\AdvancedManyToManyObjectRelation) {
                    $this->getDataForAdvancedManyToManyObjectRelation(
                        (array)$objectValue,
                        $object,
                        $key,
                        $value
                    );
                } else {
                    foreach ((array)$objectValue as $rel) {
                        $obj = AbstractObject::getById($rel->getId());
                        if (!is_null($obj)) {
                            $objKey = $this->getUniqueKey($obj);
                            $value[] = '@' . $objKey;
                        }
                    }
                }

                return $value;
            }

            return null;
        }

        $getter = 'get' . ucfirst($key);

        $fieldData = $object->$getter();

        // BLOCK
        if ($fieldDefinition instanceof Block) {
            $items = $object->$getter();
            $value = [];

            /** @var array $elements */
            foreach ($items as $elements) {
                /** @var BlockElement $element */
                foreach ($elements as $fieldName => $element) {
                    $value[] = [
                        'name' => $fieldName,
                        'type' => $element->getType(),
                        'data' => (new Expr\UninterpretedValue($element->getData()))->get(),
                    ];
                }
            }
        } else {
            $value = $fieldDefinition->getDataForEditmode($fieldData, $object);

            // TODO: refactor!
            if ($fieldDefinition instanceof DataObject\ClassDefinition\Data\Datetime
                || $fieldDefinition instanceof DataObject\ClassDefinition\Data\Date
            ) {
                $value = (new Expr\DateTimeValue($fieldData))->get();
            } elseif ($fieldDefinition instanceof DataObject\ClassDefinition\Data\Multiselect) {
                $value = (new Expr\ListValue($value))->get();
            } else {
                $value = (new Expr\UninterpretedValue($value))->get();
            }
        }

        return $value;
    }

    public function getDataForAdvancedManyToManyObjectRelation(
        array $relations,
        Concrete $sourceObj,
        string $attributeKey,
        array &$value
    ): array {
        $getter = 'get' . ucfirst($attributeKey);
        /** @var DataObject\Data\ObjectMetadata $metadata */
        $metadata = $sourceObj->$getter();
        if ($metadata instanceof DataObject\Data\ObjectMetadata) {
            $metadata = [$metadata];
        }

        foreach ($relations as $idx => $rel) {
            $relatedObj = AbstractObject::getById($rel['id']);
            if (is_null($relatedObj)) {
                continue;
            }

            $value[] = array_merge(
                [
                    '__key' => '@' . self::getUniqueKey($relatedObj),
                ],
                $metadata[$idx]->getData()
            );
        }

        return $value;
    }

    public function getDataForFieldCollectionItem(AbstractData $item): array
    {
        $fcDefinition = $item->getDefinition();

        $data = [];
        foreach ($fcDefinition->getFieldDefinitions(['suppressEnrichment' => true]) as $key => $fieldDefinition) {
            try {
                $data[$key] = $this->valueExtractorChain->getValue($item, $key, $fieldDefinition);
            } catch (NotImplementedException $e) {
                // relations
                if ($fieldDefinition instanceof AbstractRelations) {
                    $data[$key] = [];

                    $getter = 'get' . ucfirst($key);
                    $relations = $item->$getter();

                    if (!is_array($relations)) {
                        $relations = [$relations];
                    }

                    if ($fieldDefinition instanceof ManyToManyObjectRelation) {
                        foreach ($relations as $relation) {
                            $obj = AbstractObject::getById($relation->getId());
                            $objKey = $this->getUniqueKey($obj);
                            $data[$key][] = '@' . $objKey;
                        }
                    } elseif ($fieldDefinition instanceof DataObject\ClassDefinition\Data\ManyToOneRelation) {
                        if (!$relations[0]) {
                            continue;
                        }
                        $obj = AbstractObject::getById($relations[0]->getId());
                        $objKey = $this->getUniqueKey($obj);
                        $data[$key] = '@' . $objKey;
                    } else {
                        throw new NotImplementedException(
                            'Unhandled relation type found: ' . get_class($fieldDefinition)
                        );
                    }

                    continue;
                }

                // localized
                if ($fieldDefinition instanceof DataObject\ClassDefinition\Data\Localizedfields) {
                    foreach ($fieldDefinition->getFieldDefinitions() as $localizedKey => $localizedFd) {
                        $getter = 'get' . ucfirst($localizedKey);
                        $systemLanguages = Tool::getValidLanguages();
                        foreach ($systemLanguages as $language) {
                            $value = $item->$getter($language);
                            $data[$localizedKey][$language] = (new Expr\UninterpretedValue($value))->get();
                        }
                    }
                    continue;
                }

                // BLOCK
                $getter = 'get' . ucfirst($key);
                if ($fieldDefinition instanceof Block) {
                    $items = $item->$getter();

                    /** @var array $elements */
                    foreach ($items as $elements) {
                        $blockData = [];
                        /** @var BlockElement $element */
                        foreach ($elements as $fieldName => $element) {
                            $elementData = $element->getData();
                            if ($elementData instanceof Concrete) {
                                $fieldData = '@' . $this->getUniqueKey($elementData);
                            } else {
                                $fieldData = (new Expr\UninterpretedValue($elementData))->get();
                            }

                            $blockData[] = [
                                'name' => $fieldName,
                                'type' => $element->getType(),
                                'data' => $fieldData,
                            ];
                        }

                        $data[$key][] = $blockData;
                    }

                    continue;
                }

                // field data
                $fieldData = $item->$getter();
                $value = $fieldDefinition->getDataForEditmode($fieldData);

                if ($fieldDefinition instanceof DataObject\ClassDefinition\Data\Datetime
                    || $fieldDefinition instanceof DataObject\ClassDefinition\Data\Date
                ) {
                    $value = (new Expr\DateTimeValue($value))->get();
                } else {
                    $value = (new Expr\UninterpretedValue($value))->get();
                }

                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function hasFieldCollections(): bool
    {
        if ($this->object instanceof Concrete) {
            foreach ($this->object->getClass()->getFieldDefinitions() as $def) {
                if ($def instanceof Fieldcollections) {
                    return true;
                }
            }
        }

        return false;
    }

    public function hasObjectBrick(): bool
    {
        if ($this->object instanceof Concrete) {
            foreach ($this->object->getClass()->getFieldDefinitions() as $def) {
                if ($def instanceof Objectbricks) {
                    return true;
                }
            }
        }

        return false;
    }

    public function hasPimcoreProperties(): bool
    {
        return $this->object instanceof DataObject\AbstractObject && !empty($this->object->getProperties());
    }

    public function addObjectBricksForObject(AbstractObject $object, array &$classArray): void
    {
        if ($object instanceof Concrete) {
            foreach ($object->getClass()->getFieldDefinitions() as $key => $def) {
                if ($def instanceof Objectbricks) {
                    $getter = 'get' . ucfirst($key);
                    /** @var ?DataObject\Objectbrick $objectBrick */
                    $objectBrick = $object->$getter();
                    if (!$objectBrick) {
                        continue;
                    }

                    $objectBrickGetter = current($objectBrick->getBrickGetters());
                    /** @var DataObject\Objectbrick\Data\AbstractData $objectBrickHolder */
                    $objectBrickHolder = $objectBrick->$objectBrickGetter();
                    if ($objectBrickHolder) {
                        $objectBrickHolderDefinition = $objectBrickHolder->getDefinition();
                        $holderKey = self::getUniqueKey($object) . '_' . $key . '_holder';
                        $classArray[get_class($objectBrickHolder)][$holderKey]['__construct'] = [
                            '@' . self::getUniqueKey($object),
                        ];

                        foreach ($objectBrickHolderDefinition->getFieldDefinitions() as $brickKey => $brickDef) {
                            $getter = 'get' . ucfirst($brickKey);
                            $value = $objectBrickHolder->$getter();

                            if ($brickDef instanceof DataObject\ClassDefinition\Data\Datetime
                                || $brickDef instanceof DataObject\ClassDefinition\Data\Date
                            ) {
                                $value = (new Expr\DateTimeValue($value))->get();
                            }

                            $classArray[get_class($objectBrickHolder)][$holderKey][$brickKey] = $value;
                        }

                        $classArray[get_class($objectBrick)][self::getUniqueKey($object) . '_' . $key] = [
                            '__construct' => ['@' . self::getUniqueKey($object), $key],
                            $key          => '@' . self::getUniqueKey($object) . '_' . $key . '_holder',
                        ];
                    }
                }
            }
        }
    }

    public function addFieldCollectionsForObject(AbstractObject $object, array &$classArray): void
    {
        if (false === $object instanceof Concrete) {
            return;
        }

        foreach ($object->getClass()->getFieldDefinitions() as $key => $def) {
            if ($def instanceof Fieldcollections) {
                $getter = 'get' . ucfirst($key);
                /** @var Fieldcollection $fieldCollection */
                $fieldCollection = $object->$getter();

                if (empty($fieldCollection)) {
                    continue;
                }

                foreach ($fieldCollection->getItems() as $i => $item) {
                    $value = $this->getDataForFieldCollectionItem($item);
                    $itemKey = self::getUniqueKey($object) . '_' . $key . '_' . $i . '_holder';
                    $classArray[get_class($item)][$itemKey] = $value;
                    $classArray[get_class($object)][self::getUniqueKey($object)][$key][] = '@' . $itemKey;
                }
            }
        }
    }

    public function addPimcorePropertiesForObject(AbstractObject $object, array &$classArray): void
    {
        $properties = $object->getProperties();

        foreach ($properties as $property) {
            $itemKey = self::getUniqueKey($object) . '_' . $property->getName() . '_property_holder';

            $classArray[get_class($property)][$itemKey] = $this->filterVars($property);
            $classArray[get_class($object)][self::getUniqueKey($object)]['properties'][] = '@' . $itemKey;
        }
    }

    /**
     * Un-sets keys like classId, className .. see self::$ignoredFields
     */
    private function filterVars(AbstractObject|Property $child): array
    {
        $vars = $child->getObjectVars();

        foreach ($vars as $key => $var) {
            if (in_array($key, self::$ignoredFields, true)) {
                unset($vars[$key]);
            }
        }

        return $vars;
    }

    private function addSystemReferences(array &$vars): void
    {
        $parent = $this->object->getParent();
        // Special case when parent is object home
        if ($parent->getId() === 1) {
            $vars['parentId'] = 1;
        } else {
            $objKey = $this->getUniqueKey($parent);
            $parentKey = '@' . $objKey;
            $vars['parent'] = $parentKey;
        }

        $vars['key'] = (new Expr\UninterpretedValue($this->object->getKey()))->get();
        if ($this->object instanceof Concrete) {
            $vars['published'] = $this->object->getPublished();
        }
    }

    public static function getUniqueKey(AbstractObject $obj): string
    {
        $classReflect = new ReflectionClass($obj);
        $className = lcfirst($classReflect->getShortName());
        $className = strtolower($className);

        $objKey = sprintf(
            '%03d_%s_%s',
            self::getCurrentLevel($obj),
            $className,
            $obj->getKey()
        );

        // Replace any other characters to _
        return preg_replace('/[^0-9a-zA-Z]+/', '_', $objKey);
    }

    /**
     * Gets the level relative to home folder
     */
    public static function getCurrentLevel(AbstractObject $child): int
    {
        $fullPath = substr($child->getFullPath(), 1);

        return count(explode('/', $fullPath)) - 1;
    }
}
