<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Exception;

use RuntimeException;

class NotImplementedException extends RuntimeException
{
}
