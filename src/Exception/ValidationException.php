<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Exception;

use RuntimeException;

class ValidationException extends RuntimeException
{
    public static function folderIdNotInteger(): self
    {
        return new static('Folder id must be an integer');
    }

    public static function filenamePrefixNotSnakeCase(): self
    {
        return new static('Filename must be snake_case');
    }

    public static function levelNotInteger(): self
    {
        return new static('Levels must be an integer');
    }
}
