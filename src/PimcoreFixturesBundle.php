<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures;

use Basilicom\PimcoreFixtures\DependencyInjection\Compiler\PropertyHydratorChainCompilerPass;
use Basilicom\PimcoreFixtures\DependencyInjection\Compiler\ValueExtractorChainCompilerPass;
use Basilicom\PimcoreFixtures\DependencyInjection\PimcoreFixturesBundleExtension;
use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class PimcoreFixturesBundle extends AbstractPimcoreBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new PimcoreFixturesBundleExtension();
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new PropertyHydratorChainCompilerPass());
        $container->addCompilerPass(new ValueExtractorChainCompilerPass());
    }
}
