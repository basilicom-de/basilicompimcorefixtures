<?php

declare(strict_types=1);

namespace Basilicom\PimcoreFixtures\Repository;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\AbstractObject;

class FolderRepository
{
    /**
     * @return AbstractObject[]
     */
    public function getFoldersByQuery(?string $query = null): array
    {
        $folders = new DataObject\Listing();
        $folders->setObjectTypes([AbstractObject::OBJECT_TYPE_FOLDER]);

        if ($query) {
            $folders->setCondition('CONCAT(path, key) LIKE ?', '%' . $query . '%');
        }

        return $folders->getObjects();
    }
}
