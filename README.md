# Pimcore YML fixtures

Based on [Alice 3](https://github.com/nelmio/alice)

This package was originally a fork of: https://github.com/YouweGit/PimcoreFixtures

### How to install

```sh
composer require --dev basilicom/pimcore-fixtures
```
Register the bundle in `Kernel`:

```
if (in_array($this->getEnvironment(), ['test', 'dev'])
    && class_exists(\Basilicom\PimcoreFixtures\PimcoreFixturesBundle::class)
) {
    $collection->addBundle(new \Basilicom\PimcoreFixtures\PimcoreFixturesBundle());
}
```

### Usage

**Generate fixtures:**
```
bin/console basilicom:pimcore:fixtures:generate
```

**Load fixtures:**
```
bin/console basilicom:pimcore:fixtures:load <PATH-TO-FIXTURES-FOLDER>
```

**Reset all pimcore objects**
```
bin/console basilicom:pimcore:fixtures:load --force --drop-folder --exclude=ClassName,ClassName,...
```

It will also clear common tables like:
- notes
- notes_data
- edit_lock
- search_backend_data
- recyclebin
- dependencies
- versions
- custom_layouts

You can extend this list by configuring the `additionalTables` property of the command, via `services.yaml`.

### Extension

#### Property Hydrators

Custom property hydrators can be injected using Symfony's service tags:

      Basilicom\PimcoreFixtures\Alice\Generator\Hydrator\Pimcore\LocalizedFieldHydrator:
        tags:
          - { name: 'basilicom_pimcore_fixtures.property_hydrator', priority: 10 }

The hydrators need to implement the [ChainedServiceHydratorInterface](./src/Alice/Generator/Hydrator/ChainedPropertyHydratorInterface.php)
and are executed in order of their priority. Execution for a single property stops once a hydrator has handled it.

Currently, hydrator implementations come built-in for:

 * LocalizedFields
 * Blocks
 * FieldCollections
 * QuantityValues
 
 Last resort of property hydration is done by Alice's `SymfonyPropertyAccessorHydrator`.
